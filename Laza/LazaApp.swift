//
//  LazaApp.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

@main
struct LazaApp: App {
    var body: some Scene {
        WindowGroup {
            TabBarView()
        }
    }
}
