//
//  ProductCard.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 30/12/23.
//

import SwiftUI

struct ProductCard: View {
    var productImage: String
    var productName: String
    var productPrice: UInt32
    @State private var favIcon = false
    var body: some View {
        VStack {
            ZStack {
                RoundedRectangle(cornerRadius: 20)
                    .fill(Color.background2)
                Image(productImage)
                Image(systemName: favIcon ?  "heart.fill" :  "heart")
                    .font(.title2)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
                    .padding()
                    .onTapGesture {
                        favIcon.toggle()
                    }
            }
            VStack(alignment: .leading, spacing: 8) {
                Text(productName)
                    .font(.system(size: 18, weight: .regular))
                    .foregroundStyle(Color.text)
                Text(productPrice, format: .currency(code: "USD"))
                    .font(.system(size: 18, weight: .bold))
                    .foregroundStyle(Color.text)
            }
            .frame(maxWidth: .infinity, alignment: .leading)
        }
        .frame(width: 160, height: 300)
    }
}

#Preview {
    ProductCard(productImage: "product-person", productName: "Nike Sportswear Club Fleece", productPrice: 99)
}
