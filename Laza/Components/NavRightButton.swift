//
//  NavRightButton.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 30/12/23.
//

import SwiftUI

struct NavRightButton: View {
    var systemIconName: String = "arrow.right"
    var body: some View {
        HStack {
            ZStack {
                Circle()
                    .fill(Color.background2)
                    .frame(width: 44, height: 44)
                Image(systemName: systemIconName)
                    .font(.title2)
                    .foregroundStyle(Color.text)
            }
        }
        .frame(maxWidth: .infinity, alignment: .trailing)
        .padding(.horizontal)
    }
}

#Preview {
    NavRightButton()
}
