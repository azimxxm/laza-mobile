//
//  TextInput.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI


struct TextInput: View {
    var placeholder: String
    @State var name: String = ""
    @State var password: String = ""
    var body: some View {
        VStack() {
            VStack(alignment: .leading, spacing: 16) {
                Text(placeholder)
                    .font(.system(size: 14, weight: .regular))
                    .foregroundStyle(Color.textGray)
                HStack {
                    TextField("Enter your \(placeholder.lowercased())", text: $name)
                        .foregroundStyle(Color.text)
                    Image("check")
                }
                Rectangle()
                    .fill(Color.text.opacity(0.2))
                    .frame(height: 1)
            }
            .padding(.horizontal)
        }
    }
}

#Preview {
    TextInput(placeholder: "Username")
}
