//
//  CategoryHeader.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 30/12/23.
//

import SwiftUI

struct CategoryHeader: View {
    var title: String
    var rightText: String = "View All"
    var body: some View {
        HStack {
            Text(title)
                .font(.system(size: 20, weight: .medium))
                .foregroundStyle(.text)
            Spacer()
            Text(rightText)
                .font(.system(size: 18, weight: .regular))
                .foregroundStyle(.textGray)
        }
    }
}

#Preview {
    CategoryHeader(title: "Choose Brand")
}
