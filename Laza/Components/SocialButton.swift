//
//  SocialButton.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

struct SocialButton: View {
    
    var name: String
    var iocnName: String
    var backgroundColor: Color
    
    var body: some View {
        HStack {
            Image(iocnName)
            Text(name)
                .font(.system(size: 18, weight: .regular))
                .foregroundStyle(Color.white)
        }
        .frame(maxWidth: .infinity, maxHeight: 50)
        .background(backgroundColor)
        .mask(RoundedRectangle(cornerRadius: 16))
        .padding(.horizontal)
    }
}

#Preview {
    SocialButton(name: "Facebook", iocnName: "Facebook", backgroundColor: .blue)
}
