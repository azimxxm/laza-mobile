//
//  OTPCard.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 30/12/23.
//

import SwiftUI

struct OTPCard: View {
    var number: UInt8
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10)
                .stroke()
                .fill(Color.textGray)
                .frame(width: 77, height: 98)
            Text("\(number)")
                .foregroundStyle(Color.text)
                .font(.system(size: 24, weight: .medium))
        }
    }
}

#Preview {
    OTPCard(number: 7)
}
