//
//  ButtomButton.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

struct ButtomButton: View {
    var name: String
    var backgroundColor: Color = .main
    var body: some View {
        VStack {
            Text(name)
                .font(.system(size: 20, weight: .medium))
                .foregroundStyle(Color.white)
        }
        .frame(maxWidth: .infinity)
        .frame(height: 24)
        .padding()
        .background(backgroundColor)
    }
}

#Preview {
    ButtomButton(name: "Create an Account")
}
