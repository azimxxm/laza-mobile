//
//  BackButton.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

struct NavLeftButton: View {
    var systemIconName: String = "arrow.left"
    var body: some View {
        HStack {
            ZStack {
                Circle()
                    .fill(Color.background2)
                    .frame(width: 44, height: 44)
                Image(systemName: systemIconName)
                    .font(.title2)
                    .foregroundStyle(Color.text)
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(.horizontal)
    }
}

#Preview {
    NavLeftButton()
}
