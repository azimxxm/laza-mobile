//
//  BrandCard.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 30/12/23.
//

import SwiftUI

struct BrandCard: View {
    var imageName: String
    var brandName: String
    var body: some View {
        HStack {
            ZStack {
                RoundedRectangle(cornerRadius: 10)
                    .fill(Color.background)
                    .frame(width: 40, height: 40)
                Image(imageName)
                    .renderingMode(.template)
                    .foregroundStyle(Color.text)
            }
            Text(brandName)
                .font(.system(size: 16, weight: .medium))
                .foregroundStyle(Color.text)
            
        }
        .padding()
        .frame(height: 60)
        .background(Color.background2)
        .mask(RoundedRectangle(cornerRadius: 10))
    }
}

#Preview {
    BrandCard(imageName: "Adidas", brandName: "Adidas")
}
