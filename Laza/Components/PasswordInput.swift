//
//  PasswordInput.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

struct PasswordInput: View {
    var placeholder: String
    var passwordStatus: String
    var showRightStatus = true
    @State var name: String = ""
    @State var password: String = ""
    var body: some View {
        VStack() {
            VStack(alignment: .leading, spacing: 16) {
                Text(placeholder)
                    .font(.system(size: 14, weight: .regular))
                    .foregroundStyle(Color.textGray)
                HStack {
                    SecureField("Enter your \(placeholder.lowercased())", text: $name)
                    if showRightStatus {
                        Text(passwordStatus)
                            .foregroundStyle(Color.green)
                    }
                    
                }
                Rectangle()
                    .fill(Color.text.opacity(0.2))
                    .frame(height: 1)
            }
            .padding(.horizontal)
        }
    }
}

#Preview {
    PasswordInput(placeholder: "Password", passwordStatus: "Strong")
}
