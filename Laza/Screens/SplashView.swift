//
//  SplashView.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

struct SplashView: View {
    var body: some View {
        ZStack {
            Color.bgMain.ignoresSafeArea()
            Image("logo")
        }
    }
}

#Preview {
    SplashView()
}
