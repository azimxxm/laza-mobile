//
//  Screen1.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

struct Screen1: View {
    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [Color.white, Color.bgMain]), startPoint: .topLeading, endPoint: .bottomTrailing).ignoresSafeArea()
            
            Image("person-sit-down")
            VStack(spacing: 20) {
                Text("Look Good, Feel Good")
                    .font(.system(size: 24, weight: .bold))
                    .foregroundStyle(Color.text)
                Text("Create your individual & unique style and look amazing everyday.")
                    .font(.system(size: 18, weight: .medium))
                    .foregroundStyle(Color.textGray)
                HStack {
                    Text("Main")
                        .padding(20)
                        .font(.title3)
                        .frame(maxWidth: .infinity)
                        .foregroundStyle(Color.text)
                        .background(Color.background2)
                        .mask(RoundedRectangle(cornerRadius: 10))
                    Text("Women")
                        .padding(20)
                        .font(.title3)
                        .frame(maxWidth: .infinity)
                        .foregroundStyle(Color.white)
                        .background(Color.main)
                        .mask(RoundedRectangle(cornerRadius: 10))
                }
                .frame(maxWidth: .infinity)
                Text("Skip")
                    .foregroundStyle(Color.textGray)
                
            }
            .padding()
            .background(Color.background)
            .mask(RoundedRectangle(cornerRadius: 20))
            .padding(.horizontal)
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
            .padding(.vertical, 24)
        }
    }
}

#Preview {
    Screen1()
}
