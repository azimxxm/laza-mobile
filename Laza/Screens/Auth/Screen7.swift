//
//  Screen7.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 30/12/23.
//

import SwiftUI

struct Screen7: View {
    var body: some View {
        VStack {
            NavLeftButton()
            VStack(spacing: 6) {
                Text("New Password")
                    .font(.system(size: 28, weight: .medium))
                    .foregroundStyle(.text)
            }
            Spacer()
            VStack(spacing: 24) {
                PasswordInput(placeholder: "Password", passwordStatus: "Strong", showRightStatus: false)
                PasswordInput(placeholder: "Confirm Password", passwordStatus: "Strong", showRightStatus: false)
            }
            Spacer()
            Text("Please write your new password.")
                .multilineTextAlignment(.center)
                .foregroundStyle(Color.textGray)
                .padding(.horizontal)
            ButtomButton(name: "Reset Password")
        }
        .background(Color.background.ignoresSafeArea())
        
    }
}

#Preview {
    Screen7()
}
