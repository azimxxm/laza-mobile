//
//  Screen4.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

struct Screen4: View {
    @State var isOn = true
    var body: some View {
        VStack {
            NavLeftButton()
            VStack(spacing: 6) {
                Text("Welcome")
                    .font(.system(size: 28, weight: .medium))
                    .foregroundStyle(.text)
                Text("Please enter your data to continue")
                    .font(.system(size: 16, weight: .regular))
                    .foregroundStyle(Color.textGray)
            }
            Spacer()
            VStack(spacing: 24) {
                TextInput(placeholder: "Username")
                PasswordInput(placeholder: "Password", passwordStatus: "Strong")
            }
            Text("Forgot password?")
                .foregroundStyle(Color.red)
                .font(.title3)
                .frame(maxWidth: .infinity, alignment: .trailing)
                .padding()
            HStack {
                Text("Remember me")
                    .font(.system(size: 18, weight: .medium))
                Toggle("", isOn: $isOn)
            }
            .padding()
            Spacer()
            Text("By connecting your account confirm that you agree with our Term and Condition")
                .multilineTextAlignment(.center)
                .foregroundStyle(Color.textGray)
                .padding(.horizontal)
            ButtomButton(name: "Login")
        }
        .background(Color.background.ignoresSafeArea())
        
    }
}

#Preview {
    Screen4()
}
