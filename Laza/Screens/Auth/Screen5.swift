//
//  Screen5.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

struct Screen5: View {
    var body: some View {
        VStack {
            NavLeftButton()
            VStack(spacing: 6) {
                Text("Forgot Password")
                    .font(.system(size: 28, weight: .medium))
                    .foregroundStyle(.text)
                Image("lock")
            }
            Spacer()
            VStack(spacing: 24) {
                TextInput(placeholder: "Email Address")
            }
            Spacer()
            Text("Please write your email to receive a confirmation code to set a new password.")
                .multilineTextAlignment(.center)
                .foregroundStyle(Color.textGray)
                .padding(.horizontal)
            ButtomButton(name: "Confirm Mail")
        }
        .background(Color.background.ignoresSafeArea())
        
    }
}

#Preview {
    Screen5()
}
