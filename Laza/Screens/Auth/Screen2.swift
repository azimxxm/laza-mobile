//
//  Screen2.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

struct Screen2: View {
    var body: some View {
        VStack {
            NavLeftButton()
            Text("Let’s Get Started")
                .font(.system(size: 28, weight: .medium))
                .foregroundStyle(.text)
            Spacer()
            VStack(spacing: 8) {
                SocialButton(name: "Facebook", iocnName: "Facebook", backgroundColor: Color(red: 0.26, green: 0.4, blue: 0.7))
                SocialButton(name: "Twitter", iocnName: "Twitter", backgroundColor: Color(red: 0.11, green: 0.63, blue: 0.95))
                SocialButton(name: "Google", iocnName: "Google",  backgroundColor: Color(red: 0.92, green: 0.26, blue: 0.21))
            }
            Spacer()
            HStack {
                Text("Already have an account?")
                    .foregroundStyle(Color.textGray)
                Button("Signin") {
                    print("Signin")
                }
                .foregroundStyle(.text)
            }
            ButtomButton(name: "Create an Account")
        }
        .background(Color.background.ignoresSafeArea())
        
    }
    
}

#Preview {
    Screen2()
}
