//
//  Screen6.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 30/12/23.
//

import SwiftUI

struct Screen6: View {
    var body: some View {
        VStack {
            NavLeftButton()
            VStack(spacing: 6) {
                Text("Verification Code")
                    .font(.system(size: 28, weight: .medium))
                    .foregroundStyle(.text)
                Image("lock")
            }
            Spacer()
                .frame(height: 60)
            HStack {
                OTPCard(number: 7)
                OTPCard(number: 4)
                OTPCard(number: 2)
                OTPCard(number: 3)
            }
            Spacer()
            HStack(spacing: 6) {
                Text("00:20")
                    .foregroundStyle(.text)
                Text("resend confirmation code.")
                    .multilineTextAlignment(.center)
                    .foregroundStyle(Color.textGray)
            }
            .padding(.horizontal)
            ButtomButton(name: "Confirm Code")
        }
        .background(Color.background.ignoresSafeArea())
        
    }
}

#Preview {
    Screen6()
}
