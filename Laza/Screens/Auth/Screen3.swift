//
//  Screen3.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 29/12/23.
//

import SwiftUI

struct Screen3: View {
    @State var isOn = true
    var body: some View {
        VStack {
            NavLeftButton()
            Text("Sign Up")
                .font(.system(size: 28, weight: .medium))
                .foregroundStyle(.text)
            Spacer()
            VStack(spacing: 24) {
                TextInput(placeholder: "Username")
                PasswordInput(placeholder: "Password", passwordStatus: "Strong")
                TextInput(placeholder: "Email Address")
            }
            HStack {
                Text("Remember me")
                    .font(.system(size: 18, weight: .medium))
                Toggle("", isOn: $isOn)
            }
            .padding()
            Spacer()
            ButtomButton(name: "Sign Up")
        }
        .background(Color.background.ignoresSafeArea())
        
    }
}

#Preview {
    Screen3()
}
