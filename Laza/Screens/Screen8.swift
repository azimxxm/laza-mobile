//
//  Screen8.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 30/12/23.
//

import SwiftUI

struct Screen8: View {
    @State var text = ""
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack {
                HStack {
                    NavLeftButton()
                    NavRightButton(systemIconName: "bag")
                }
                VStack(alignment: .leading) {
                    Text("Hello")
                        .font(.system(size: 24, weight: .medium))
                        .foregroundStyle(Color.text)
                    Text("Welcome to Laza.")
                        .font(.system(size: 18, weight: .regular))
                        .foregroundStyle(Color.textGray)
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.horizontal)
                
                HStack {
                    ZStack {
                        RoundedRectangle(cornerRadius: 16)
                            .fill(Color.background2)
                            .frame(height: 60)
                        HStack {
                            Image(systemName: "magnifyingglass")
                                .font(.title2)
                                .foregroundStyle(Color.textGray)
                            TextField("Search...", text: $text)
                        }
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.horizontal)
                    }
                    ZStack {
                        RoundedRectangle(cornerRadius: 16)
                            .fill(Color.main)
                            .frame(width: 60, height: 60)
                        Image(systemName: "mic")
                            .font(.title2)
                            .foregroundStyle(Color.white)
                    }
                }
                .padding()
                
                CategoryHeader(title: "Choose Brand")
                
                    .frame(maxWidth: .infinity)
                    .padding()
                
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                        BrandCard(imageName: "Adidas", brandName: "Adidas")
                        BrandCard(imageName: "fila", brandName: "Fila")
                        BrandCard(imageName: "nike", brandName: "Nike")
                        BrandCard(imageName: "puma-logo", brandName: "Puma")
                    }
                }
                .padding(.horizontal)
                CategoryHeader(title: "New Arraival")
                    .padding()
                
                LazyVGrid(columns: [
                    GridItem(.flexible(minimum: 100, maximum: 180)),
                    GridItem(.flexible(minimum: 100, maximum: 180)),
                ], spacing: 16, content: {
                    ProductCard(productImage: "product-person", productName: "Nike Sportswear Club Fleece", productPrice: 99)
                    ProductCard(productImage: "product-person2", productName: "Nike Sportswear Club Fleece", productPrice: 99)
                    ProductCard(productImage: "product-person3", productName: "Nike Sportswear Club Fleece", productPrice: 99)
                    ProductCard(productImage: "product-person4", productName: "Nike Sportswear Club Fleece", productPrice: 99)
                })
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        }
        .padding(.bottom, 50)
    }
}

#Preview {
    Screen8()
}
