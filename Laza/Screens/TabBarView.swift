//
//  TabBarView.swift
//  Laza
//
//  Created by Azimjon Abdurasulov on 30/12/23.
//

import SwiftUI

struct TabBarView: View {
    var body: some View {
        TabView {
            Screen8()
                .tabItem {
                    Label("Home", systemImage: "house")
                }
            Text("Wishlist")
                .tabItem {
                    Label("Wishlist", systemImage: "heart")
                }
            
            Text("Profile")
                .tabItem {
                    Label("Profile", systemImage: "person")
                }
        }
        
    }
}

#Preview {
    TabBarView()
}
